<?php
namespace Quizz\Bundle\ModelBundle;

final class MotiveType
{
    const MOTIVE_SPAM = 0;
    const MOTIVE_ABUSIVE = 1;
    const MOTIVE_OTHER = 2;

    static function getMotivesList()
    {
        return array(
            self::MOTIVE_SPAM => 'spam',
            self::MOTIVE_ABUSIVE => 'abusive',
            self::MOTIVE_OTHER => 'other'
        );
    }
}
