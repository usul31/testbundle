<?php
namespace Quizz\Bundle\ModelBundle\Event;

use Quizz\Bundle\ModelBundle\Model\QuizzInterface;
use Symfony\Component\EventDispatcher\Event;

class QuizzEvent extends Event
{
    private $quizz;

    public function __construct(QuizzInterface $quizz)
    {
        $this->quizz = $quizz;
    }

    /**
     * @return QuizzInterface
     */
    public function getQuizz()
    {
        return $this->quizz;
    }
}
