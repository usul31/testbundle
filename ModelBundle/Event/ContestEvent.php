<?php
namespace Quizz\Bundle\ModelBundle\Event;

use Symfony\Component\EventDispatcher\Event;
use Quizz\Bundle\ModelBundle\Model\ContestInterface;

class ContestEvent extends Event
{
    private $contest;

    public function __construct(ContestInterface $contest)
    {
        $this->contest = $contest;
    }

    /**
     * @return ContestInterface
     */
    public function getContest()
    {
        return $this->contest;
    }
}
