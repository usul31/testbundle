<?php
namespace Quizz\Bundle\ModelBundle\Event;

use Quizz\Bundle\ModelBundle\Model\AnswerInterface;
use Symfony\Component\EventDispatcher\Event;

/**
 * An event that occurs related to a vote.
 */
class AnswerEvent extends Event
{
    private $answer;

    /**
     * Constructs an event.
     *
     * @param \Quizz\Bundle\ModelBundle\AnswerInterface $answer
     */
    public function __construct(AnswerInterface $answer)
    {
        $this->answer = $answer;
    }

    /**
     * Returns the answer for the event.
     *
     * @return \Quizz\Bundle\ModelBundle\AnswerInterface
     */
    public function getAnswer()
    {
        return $this->answer;
    }
}
