<?php
namespace Quizz\Bundle\ModelBundle;

final class Events
{
    /**
     * User events
     */
    const USER_SAVE_COMPLETE = 'quizz_model.user.save_complete';
    const USER_POST_LOAD = 'quizz_model.user.post_load';
    const USER_PRE_DELETE = 'quizz_model.user.pre_delete';

    const QUIZZ_CREATE = 'quizz_model.quizz.create';
    const QUIZZ_NEW_PRE_PERSIST = 'quizz_model.quizz.new_pre_persist';
    const QUIZZ_PRE_PERSIST = 'quizz_model.quizz.pre_persist';
    const QUIZZ_NEW_POST_PERSIST = 'quizz_model.quizz.new_post_persist';
    const QUIZZ_POST_PERSIST = 'quizz_model.quizz.post_persist';
    const QUIZZ_PRE_DELETE = 'quizz_model.quizz.pre_delete';

    const CONTEST_CREATE = 'quizz_model.contest.create';
    const CONTEST_NEW_PRE_PERSIST = 'quizz_model.contest.new_pre_persist';
    const CONTEST_PRE_PERSIST = 'quizz_model.contest.pre_persist';
    const CONTEST_NEW_POST_PERSIST = 'quizz_model.contest.new_post_persist';
    const CONTEST_POST_PERSIST = 'quizz_model.contest.post_persist';
    const CONTEST_PRE_DELETE = 'quizz_model.contest.pre_delete';

    const ANSWER_CREATE = 'quizz_model.answer.create';
    const ANSWER_NEW_PRE_PERSIST = 'quizz_model.answer.new_pre_persist';
    const ANSWER_PRE_PERSIST = 'quizz_model.answer.pre_persist';
    const ANSWER_NEW_POST_PERSIST = 'quizz_model.answer.new_post_persist';
    const ANSWER_POST_PERSIST = 'quizz_model.answer.post_persist';
    const ANSWER_PRE_DELETE = 'quizz_model.answer.pre_delete';
}
