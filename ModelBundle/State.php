<?php

namespace Quizz\Bundle\ModelBundle;

final class State
{
    const STATE_VISIBLE = 0;
    const STATE_DELETED = 1;
    const STATE_SPAM = 2;
    const STATE_PENDING = 3;
}
