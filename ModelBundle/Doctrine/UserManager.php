<?php

/*
 * This file is part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Quizz\Bundle\ModelBundle\Doctrine;

use Quizz\Bundle\ModelBundle\Events;
use FOS\UserBundle\Doctrine\UserManager as FOSUserManager;
use Quizz\Bundle\ModelBundle\Event\UserEvent;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use FOS\UserBundle\Model\UserInterface;
use FOS\UserBundle\Util\CanonicalizerInterface;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;
use Doctrine\Common\Persistence\ObjectManager;

class UserManager extends FOSUserManager
{

    protected $eventDispatcher = null;

    /**
     * Constructor.
     *
     * @param EncoderFactoryInterface $encoderFactory
     * @param CanonicalizerInterface  $usernameCanonicalizer
     * @param CanonicalizerInterface  $emailCanonicalizer
     * @param ObjectManager           $om
     * @param string                  $class
     * @param EventDispatcher					$eventDispatcher
     */
    public function __construct(EncoderFactoryInterface $encoderFactory, CanonicalizerInterface $usernameCanonicalizer, CanonicalizerInterface $emailCanonicalizer, ObjectManager $om, $class, EventDispatcherInterface $eventDispatcher)
    {
        parent::__construct($encoderFactory, $usernameCanonicalizer, $emailCanonicalizer, $om, $class);

        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * Updates a user.
     * Dispatch some events.
     *
     * @param UserInterface $user
     * @param Boolean       $andFlush Whether to flush the changes (default true)
     */
    public function updateUser(UserInterface $user, $andFlush = true)
    {
        parent::updateUser($user, $andFlush);
        $this->eventDispatcher->dispatch(Events::USER_SAVE_COMPLETE, new UserEvent($user));
    }

    /**
     * Overload loadUserByUsername as this function is used by oauth and we need to be able to log in with email.
     * @param string $username
     *
     * @return UserInterface
     */
    public function loadUserByUsername($username)
    {
        $user = $this->findUserByUsernameOrEmail($username);

        if (!$user) {
            throw new \Exception(sprintf('No user with name "%s" was found.', $username));
        }

        return $user;
    }
}
