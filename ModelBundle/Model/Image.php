<?php
namespace Quizz\Bundle\ModelBundle\Model;

use Symfony\Component\HttpFoundation\File\UploadedFile;

abstract class Image
{
    /**
     * Temporary uploaded file.
     * @var UploadedFile
     */
    protected $picture;

    /**
     * Upload directory.
     * @var string
     */
    protected $uploadDir;

    /**
     * Upload root directory.
     * @var string
     */
    protected $uploadRootDir;

    /**
     * Upload picture
     * @return bool
     */
    public function upload()
    {
        // no new image uploaded.
        if (null === $this->getPicture())
        {
            return false;
        }
        $key = sha1(uniqid(mt_rand(), true));
        $filename = $key . '.' . $this->getPicture()->guessExtension();

        $uploadDir = DIRECTORY_SEPARATOR . implode(DIRECTORY_SEPARATOR, array_slice(str_split($filename, 1), 0, 10));
        // move takes the target directory and then the
        // target filename to move to
        $this->getPicture()->move($this->getUploadRootDir() . DIRECTORY_SEPARATOR . $this->getUploadDir() . $uploadDir, $filename);
        $this->picture = null;

        return [
            'image' => $this->getUploadDir() . $uploadDir . DIRECTORY_SEPARATOR . $filename,
            'path' => $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $this->getUploadDir() . $uploadDir . DIRECTORY_SEPARATOR . $filename
        ];
    }

    /**
     * @param $picture
     * @return $this
     */
    public function setPicture(UploadedFile $picture = null)
    {
        $this->picture = $picture;
        return $this;
    }

    /**
     * @return string
     */
    public function getPicture()
    {
        return $this->picture;
    }

    /**
     * Set upload dir
     * @param null $path
     * @return $this
     */
    public function setUploadDir($path = null)
    {
        $this->uploadDir = $path;
        return $this;
    }

    /**
     * Get upload dir
     */
    public function getUploadDir()
    {
        return $this->uploadDir;
    }

    /**
     * Set upload root dir
     * @param null $path
     * @return $this
     */
    public function setUploadRootDir($path = null)
    {
        $this->uploadRootDir = $path;
        return $this;
    }

    /**
     * Get upload root dir
     */
    public function getUploadRootDir()
    {
        return $this->uploadRootDir;
    }
}
