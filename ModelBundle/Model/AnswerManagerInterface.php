<?php
namespace Quizz\Bundle\ModelBundle\Model;

/**
 * Manages voting scores for spot.
 */
interface AnswerManagerInterface
{
    /**
     * Returns the class of the answer object.
     *
     * @return string
     */
    public function getClass();

    /**
     * Creates a answer object.
     *
     * @param  QuizzInterface $quizz
     * @param  ContestInterface $contest
     * @return AnswerInterface
     */
    public function createAnswer(QuizzInterface $quizz, ContestInterface $contest);

    /**
     * Persists an answer.
     *
     * @param  AnswerInterface $answer
     * @return void
     */
    public function saveAnswer(AnswerInterface $answer);

    /**
     * Finds an answer by specified criteria.
     *
     * @param  array         $criteria
     * @return AnswerInterface
     */
    public function findAnswerBy(array $criteria);

    /**
     * Finds a answer by id.
     *
     * @param  $id
     * @return AnswerInterface
     */
    public function findAnswerById($id);

    /**
     * Checks if the answer was already persisted before, or if it's a new one.
     * @param AnswerInterface $answer
     * @return boolean True, if it's a new answer
     */
    public function isNewAnswer(AnswerInterface $answer);
}
