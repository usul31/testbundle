<?php
namespace Quizz\Bundle\ModelBundle\Model;

use Quizz\Bundle\ModelBundle\Events;
use Quizz\Bundle\ModelBundle\Event\ContestEvent;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Abstract Contest Manager.
 *
 */
abstract class ContestManager implements ContestManagerInterface
{
    protected $dispatcher;

    public function __construct(EventDispatcherInterface $dispatcher)
    {
        $this->dispatcher = $dispatcher;
    }

    /**
     * @param  string          $id
     * @return ContestInterface
     */
    public function findContestById($id)
    {
        return $this->findContestBy(array('id' => $id));
    }

    /**
     * @inherit
     */
    public function createContest($id = null)
    {
        $class = $this->getClass();
        $contest = new $class;

        if (null !== $id) {
            $contest->setId($id);
        }

        $event = new ContestEvent($contest);
        $this->dispatcher->dispatch(Events::CONTEST_CREATE, $event);

        return $contest;
    }

    /**
     * @inherit.
     */
    public function saveContest(ContestInterface $contest)
    {
        $event = new ContestEvent($contest);
        $isNewContest = $this->isNewContest($contest);
        if ($isNewContest)  {
            $this->dispatcher->dispatch(Events::CONTEST_NEW_PRE_PERSIST, $event);
        }
        else {
            $this->dispatcher->dispatch(Events::CONTEST_PRE_PERSIST, $event);
        }

        $this->doSaveContest($contest);

        $event = new ContestEvent($contest);
        if ($isNewContest)  {
            $this->dispatcher->dispatch(Events::CONTEST_NEW_POST_PERSIST, $event);
        }
        else {
            $this->dispatcher->dispatch(Events::CONTEST_POST_PERSIST, $event);
        }
    }

    /**
     * Delete a contest.
     * @param ContestInterface $contest
     */
    function deleteContest(ContestInterface $contest)
    {
        $event = new ContestEvent($contest);
        $this->dispatcher->dispatch(Events::CONTEST_PRE_DELETE, $event);
        $this->doDeleteContest($contest);
    }
    /**
     * Performs the persistence of the contest.
     *
     * @abstract
     * @param ContestInterface $contest
     */
    abstract protected function doSaveContest(ContestInterface $contest);
}
