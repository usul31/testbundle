<?php
namespace Quizz\Bundle\ModelBundle\Model;

abstract class FavoriteContest {
    /**
     * @var string contest
     */
    protected $contest;

    /**
     * @var string contest label
     */
    protected $label;

    /**
     * @var string contest status
     */
    protected $status;

    /**
     * @var date
     */
    protected $created;


    /**
     * Constructor.
     */
    function __construct()
    {
        $this->created = new \DateTime();
    }


    /**
     * @return string
     */
    public function getContest()
    {
        return $this->contest;
    }

    /**
     * @param string $contest
     * @return $this
     */
    public function setContest($contest)
    {
        $this->contest = $contest;
        return $this;
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @param string $label
     * @return $this
     */
    public function setLabel($label)
    {
        $this->label = $label;
        return $this;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }
}
