<?php
namespace Quizz\Bundle\ModelBundle\Model;

use Quizz\Bundle\ModelBundle\Events;
use Quizz\Bundle\ModelBundle\Event\QuizzEvent;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Abstract quizz Manager.
 *
 */
abstract class QuizzManager implements QuizzManagerInterface
{
    protected $dispatcher;

    public function __construct(EventDispatcherInterface $dispatcher)
    {
        $this->dispatcher = $dispatcher;
    }

    /**
     * @param  string          $id
     * @return QuizzInterface
     */
    public function findQuizzById($id)
    {
        return $this->findQuizzBy(array('id' => $id));
    }

    /**
     * @inherit
     */
    public function createQuizz($id = null)
    {
        $class = $this->getClass();
        $quizz = new $class;

        if (null !== $id) {
            $quizz->setId($id);
        }

        $event = new QuizzEvent($quizz);
        $this->dispatcher->dispatch(Events::QUIZZ_CREATE, $event);

        return $quizz;
    }

    /**
     * @inherit.
     */
    public function saveQuizz(QuizzInterface $quizz)
    {
        $event = new QuizzEvent($quizz);
        $isNewQuizz = $this->isNewQuizz($quizz);
        if ($isNewQuizz)  {
            $this->dispatcher->dispatch(Events::QUIZZ_NEW_PRE_PERSIST, $event);
        }
        else {
            $this->dispatcher->dispatch(Events::QUIZZ_PRE_PERSIST, $event);
        }

        $this->doSaveQuizz($quizz);

        $event = new QuizzEvent($quizz);
        if ($isNewQuizz)  {
            $this->dispatcher->dispatch(Events::QUIZZ_NEW_POST_PERSIST, $event);
        }
        else {
            $this->dispatcher->dispatch(Events::QUIZZ_POST_PERSIST, $event);
        }
    }

    /**
     * Delete a quizz.
     * @param QuizzInterface $quizz
     */
    function deleteQuizz(QuizzInterface $quizz)
    {
        $event = new QuizzEvent($quizz);
        $this->dispatcher->dispatch(Events::QUIZZ_PRE_DELETE, $event);
        $this->doDeleteQuizz($quizz);
    }
    /**
     * Performs the persistence of the quizz.
     *
     * @abstract
     * @param QuizzInterface $quizz
     */
    abstract protected function doSaveQuizz(QuizzInterface $quizz);
}

