<?php
namespace Quizz\Bundle\ModelBundle\Model;

/**
 * Interface to be implemented by quizz managers.
 *
 */
interface QuizzManagerInterface
{
    /**
     * @param  string          $id
     * @return QuizzInterface
     */
    public function findQuizzById($id);

    /**
     * Finds one quizz by the given criteria
     *
     * @param  array           $criteria
     * @return QuizzInterface
     */
    public function findQuizzBy(array $criteria);

    /**
     * Finds quizzes by the given criteria
     *
     * @param array $criteria
     *
     * @return array of QuizzInterface
     */
    public function findQuizzesBy(array $criteria);

    /**
     * Creates an empty quizz instance
     *
     * @param  bool   $id
     * @return QuizzInterface
     */
    public function createQuizz($id = null);

    /**
     * Saves a quizz
     *
     * @param QuizzInterface $quizz
     */
    public function saveQuizz(QuizzInterface $quizz);

    /**
     * Checks if the quizz was already persisted before, or if it's a new one.
     *
     * @param QuizzInterface $quizz
     *
     * @return boolean True, if it's a new quizz
     */
    public function isNewQuizz(QuizzInterface $quizz);

    /**
     * Returns the quizz manager fully qualified class name
     *
     * @return string
     */
    public function getClass();
}
