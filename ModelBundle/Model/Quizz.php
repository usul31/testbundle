<?php
namespace Quizz\Bundle\ModelBundle\Model;

/**
 * Storage agnostic user object
 */
abstract class Quizz extends Image implements QuizzInterface
{
    /**
     * @var integer
     *
     **/
    protected $id;

    /**
     * @var string
     */
    protected $question;

    /**
     * @var string
     */
    protected $questionImage;

    /**
     * @var string
     */
    protected $questionImagePath;

    /**
     * @var string
     */
    protected $questionType;

    /**
     * @var boolean;
     */
    protected $private;

    /**
     * @var string
     */
    protected $privateCode;

    /**
     * @var string
     */
    protected $responseLabel1;

    /**
     * @var string
     */
    protected $responseLabel2;

    /**
     * @var string
     */
    protected $responseLabel3;

    /**
     * @var string
     */
    protected $responseLabel4;

    /**
     * Currect response name
     * @var string
     */
    protected $correctResponse;

    /**
     * @var string
     */
    protected $responseChoice1;

    /**
     * @var string
     */
    protected $responseChoice2;

    /**
     * @var string
     */
    protected $responseChoice3;

    /**
     * @var string
     */
    protected $responseChoice4;

    /**
     * @var string
     */
    protected $responseChoice1Path;

    /**
     * @var string
     */
    protected $responseChoice2Path;

    /**
     * @var string
     */
    protected $responseChoice3Path;

    /**
     * @var string
     */
    protected $responseChoice4Path;

    /**
     * @var int
     */
    protected $nbResponseChoice1;

    /**
     * @var int
     */
    protected $nbResponseChoice2;

    /**
     * @var int
     */
    protected $nbResponseChoice3;

    /**
     * @var int
     */
    protected $nbResponseChoice4;

    /**
     * @var string
     */
    protected $response;

    /**
     * @var hash
     **/
    protected $tags = array();

    /**
     * @var DateTime
     */
    protected $created;

    /**
     * @var DateTime
     */
    protected $changed;

    /**
     * @var mixte current user response.
     */
    protected $currentUserResponse;

    /**
     *
     * Public constructor for Quizz
     */
    public function __construct()
    {
        $this->created = new \DateTime();
        $this->changed = new \DateTime();
        $this->private = false;
        $this->nbResponseChoice1 = 0;
        $this->nbResponseChoice2 = 0;
        $this->nbResponseChoice3 = 0;
        $this->nbResponseChoice4 = 0;
        $this->currentUserResponse = null;
    }

    /**
     * Set current user response.
     * @param $response
     * @return $this
     */
    public function setCurrentUserResponse($response)
    {
        $this->currentUserResponse = $response;
        return $this;
    }

    public function getCurrentUserResponse()
    {
        return $this->currentUserResponse;
    }

    /**
     * Set technical ID.
     * @param $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * Get technical ID.
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    public function setResponse($response)
    {
        $this->response = $response;
        return $this;
    }

    /**
     * Get response.
     * @return string
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * Set question
     * @param $question
     * @return $this
     */
    public function setQuestion($question)
    {
        $this->question = $question;
        return $this;
    }

    /**
     * Get question.
     * @return string
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * set first response label choice.
     * @param $responseLabel1
     * @return $this
     */
    public function setResponseLabel1($responseLabel1)
    {
        $this->responseLabel1 = $responseLabel1;
        return $this;
    }

    /**
     * Get second response label choice.
     * @return string
     */
    public function getResponseLabel2()
    {
        return $this->responseLabel2;
    }

    /**
     * set second response label choice.
     * @param $responseLabel2
     * @return $this
     */
    public function setResponseLabel2($responseLabel2)
    {
        $this->responseLabel2 = $responseLabel2;
        return $this;
    }

    /**
     * Get third response label choice.
     * @return string
     */
    public function getResponseLabel3()
    {
        return $this->responseLabel3;
    }

    /**
     * set third response label choice.
     * @param $responseLabel3
     * @return $this
     */
    public function setResponseLabel3($responseLabel3)
    {
        $this->responseLabel3 = $responseLabel3;
        return $this;
    }

    /**
     * Get fourth response label choice.
     * @return string
     */
    public function getResponseLabel4()
    {
        return $this->responseLabel4;
    }

    /**
     * set fourth response label choice.
     * @param $responseLabel4
     * @return $this
     */
    public function setResponseLabel4($responseLabel4)
    {
        $this->responseLabel4 = $responseLabel4;
        return $this;
    }

    /**
     * Get correct response choice.
     * @return string
     */
    public function getCorrectResponse()
    {
        return $this->correctResponse;
    }

    /**
     * set correct response choice.
     * @param $correctResponse
     * @return $this
     */
    public function setCorrectResponse($correctResponse)
    {
        $this->correctResponse = $correctResponse;
        return $this;
    }

    /**
     * Get first response label choice.
     * @return string
     */
    public function getResponseLabel1()
    {
        return $this->responseLabel1;
    }

    /**
     * set first response choice.
     * @param $response1
     * @return $this
     */
    public function setResponseChoice1($response1)
    {
        $this->responseChoice1 = $response1;
        return $this;
    }

    /**
     * Get first response choice.
     * @return string
     */
    public function getResponseChoice1()
    {
        return $this->responseChoice1;
    }

    /**
     * set second response choice.
     * @param $response2
     * @return $this
     */
    public function setResponseChoice2($response2)
    {
        $this->responseChoice2 = $response2;
        return $this;
    }

    /**
     * Get second response choice.
     * @return string
     */
    public function getResponseChoice2()
    {
        return $this->responseChoice2;
    }

    /**
     * set third response choice.
     * @param $response3
     * @return $this
     */
    public function setResponseChoice3($response3)
    {
        $this->responseChoice3 = $response3;
        return $this;
    }

    /**
     * Get third response choice.
     * @return string
     */
    public function getResponseChoice3()
    {
        return $this->responseChoice3;
    }

    /**
     * set fourth response choice.
     * @param $response4
     * @return $this
     */
    public function setResponseChoice4($response4)
    {
        $this->responseChoice4 = $response4;
        return $this;
    }

    /**
     * Get fourth response choice.
     * @return string
     */
    public function getResponseChoice4()
    {
        return $this->responseChoice4;
    }

    /**
     * Set question type.
     * @param $questionType
     * @return $this
     */
    public function setQuestionType($questionType)
    {
        $this->questionType = $questionType;
        return $this;
    }

    /**
     * Get question type.
     * @return string
     */
    public function getQuestionType()
    {
        return $this->questionType;
    }

    /**
     * Set nb response choice 1.
     * @param $nb
     * @return $this
     */
    public function setNbResponseChoice1($nb)
    {
        $this->nbResponseChoice1 = (int) $nb;
        return $this;
    }

    /**
     * Get nb response choice 1
     * @return int
     */
    public function getNbResponseChoice1()
    {
        return $this->nbResponseChoice1;
    }

    /**
     * Increment nb of response choice 1.
     */
    public function incNbResponseChoice1()
    {
        if (is_int($this->nbResponseChoice1) && !empty($this->nbResponseChoice1)) {
            $this->nbResponseChoice1++;
        }
        else {
            $this->nbResponseChoice1 = 1;
        }
    }

    /**
     * Decrement nb of response choice 1.
     */
    public function decNbResponseChoice1()
    {
        if (is_int($this->nbResponseChoice1) && !empty($this->nbResponseChoice1)) {
            $this->nbResponseChoice1--;
        }
        else {
            $this->nbResponseChoice1 = 0;
        }
    }

    /**
     * Set nb response choice 2.
     * @param $nb
     * @return $this
     */
    public function setNbResponseChoice2($nb)
    {
        $this->nbResponseChoice2 = (int) $nb;
        return $this;
    }

    /**
     * Get nb response choice 2
     * @return int
     */
    public function getNbResponseChoice2()
    {
        return $this->nbResponseChoice2;
    }

    /**
     * Increment nb of response choice 2.
     */
    public function incNbResponseChoice2()
    {
        if (is_int($this->nbResponseChoice2) && !empty($this->nbResponseChoice2)) {
            $this->nbResponseChoice2++;
        }
        else {
            $this->nbResponseChoice2 = 1;
        }
    }

    /**
     * Decrement nb of response choice 2.
     */
    public function decNbResponseChoice2()
    {
        if (is_int($this->nbResponseChoice2) && !empty($this->nbResponseChoice2)) {
            $this->nbResponseChoice2--;
        }
        else {
            $this->nbResponseChoice2 = 0;
        }
    }

    /**
     * Set nb response choice 3.
     * @param $nb
     * @return $this
     */
    public function setNbResponseChoice3($nb)
    {
        $this->nbResponseChoice3 = (int) $nb;
        return $this;
    }

    /**
     * Get nb response choice 3
     * @return int
     */
    public function getNbResponseChoice3()
    {
        return $this->nbResponseChoice3;
    }

    /**
     * Increment nb of response choice 3.
     */
    public function incNbResponseChoice3()
    {
        if (is_int($this->nbResponseChoice3) && !empty($this->nbResponseChoice3)) {
            $this->nbResponseChoice3++;
        }
        else {
            $this->nbResponseChoice3 = 1;
        }
    }

    /**
     * Decrement nb of response choice 3.
     */
    public function decNbResponseChoice3()
    {
        if (is_int($this->nbResponseChoice3) && !empty($this->nbResponseChoice3)) {
            $this->nbResponseChoice3--;
        }
        else {
            $this->nbResponseChoice3 = 0;
        }
    }

    /**
     * Set nb response choice 4.
     * @param $nb
     * @return $this
     */
    public function setNbResponseChoice4($nb)
    {
        $this->nbResponseChoice4 = (int) $nb;
        return $this;
    }

    /**
     * Get nb response choice 4
     * @return int
     */
    public function getNbResponseChoice4()
    {
        return $this->nbResponseChoice4;
    }

    /**
     * Increment nb of response choice 4.
     */
    public function incNbResponseChoice4()
    {
        if (is_int($this->nbResponseChoice4) && !empty($this->nbResponseChoice4)) {
            $this->nbResponseChoice4++;
        }
        else {
            $this->nbResponseChoice4 = 1;
        }
    }

    /**
     * Decrement nb of response choice 4.
     */
    public function decNbResponseChoice4()
    {
        if (is_int($this->nbResponseChoice4) && !empty($this->nbResponseChoice4)) {
            $this->nbResponseChoice4--;
        }
        else {
            $this->nbResponseChoice4 = 0;
        }
    }

    /**
     * Set Question image.
     * @param $questionImage
     * @return $this
     */
    public function setQuestionImage($questionImage)
    {
        $this->questionImage = $questionImage;
        return $this;
    }

    /**
     * Get question image.
     * @return string
     */
    public function getQuestionImage()
    {
        return $this->questionImage;
    }

    /**
     * Set Question image path.
     * @param $questionImagePath
     * @return $this
     */
    public function setQuestionImagePath($questionImagePath)
    {
        $this->questionImagePath = $questionImagePath;
        return $this;
    }

    /**
     * Get question image path.
     * @return string
     */
    public function getQuestionImagePath()
    {
        return $this->questionImagePath;
    }
}
