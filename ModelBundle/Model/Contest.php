<?php
namespace Quizz\Bundle\ModelBundle\Model;

use FOS\UserBundle\Model\UserInterface;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

abstract class Contest extends Image implements ContestInterface
{
    /**
     * @var integer
     *
     **/
    protected $id;

    /**
     * @var string
     */
    protected $label;

    /**
     * @var array
     */
    protected $quizzes;

    /**
     * @var boolean;
     */
    protected $private;

    /**
     * @var string
     */
    protected $privateCode;

    /**
     * @var boolean;
     */
    protected $published;

    /**
     * @var DateTime
     */
    protected $expiredAt;

    /**
     * @var DateTime
     */
    protected $created;

    /**
     * @var DateTime
     */
    protected $changed;

    /**
     * @var Coordinates
     */
    protected $coordinates;

    /**
     * @var float
     **/
    protected $accuracy;

    /**
     * @var Address
     */
    protected $address;

    /**
     * @var FOS/UserBundle/Model/UserInterface
     */
    protected $author;

    /**
     * @MongoDB\Distance
     * @var distance
     */
    protected $distance;

    /**
     * @var string
     */
    protected $image;

    /**
     * @var string
     */
    protected $imagePath;

    /**
     *
     * Public constructor for Quizz
     */
    public function __construct()
    {
        $this->created = new \DateTime();
        $this->changed = new \DateTime();
        $this->private = false;
        $this->quizzes = array();
    }

    /**
     * Set expiration date.
     * @param \DateTime $expireAt
     * @return $this
     */
    public function setExpireAt(\DateTime $expireAt = null)
    {
        $this->expireAt = $expireAt;
        return $this;
    }

    /**
     * Get expiration date.
     * @return DateTime
     */
    public function getExpireAt()
    {
        return $this->expiredAt;
    }

    /**
     * Set published.
     * @param $published
     * @return $this
     */
    public function setPublished($published)
    {
        $this->published = (bool) $published;
        return $this;
    }

    /**
     * Is contest published ?
     * @return bool
     */
    public function isPublished()
    {
        return (bool) $this->published;
    }
    /**
     * Set label.
     * @param $label
     * @return $this
     */
    public function setLabel($label)
    {
        $this->label = $label;
        return $this;
    }

    /**
     * Get label.
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * Get quizz
     * @return array
     */
    public function getQuizzes()
    {
        return $this->quizzes;
    }

    /**
     * Order quizzes related to $quizzes array.
     * @param array $quizzes
     * @return $this
     */
    public function orderQuizzes(array $quizzes = array())
    {
        if (is_object($this->quizzes)) {
            $currentQuizzes = $this->quizzes->toArray();
            if (!empty($quizzes))
            {
                $sortFunction = function(Quizz $a, Quizz $b) use ($quizzes) {
                    $posA = (int) array_search((string) $a->getId(), $quizzes);
                    $posB = (int) array_search((string) $b->getId(), $quizzes);
                    return ($posA < $posB) ? -1 : 1;
                };
                usort($currentQuizzes, $sortFunction);
                $this->quizzes = $currentQuizzes;
            }
        }
        return $this;
    }

    /**
     * Add quizz to the contest.
     * @param QuizzInterface $quizz
     * @param null $position
     */
    public function addQuizz(QuizzInterface $quizz, $position = null)
    {
        // if position is given.
        if (is_int($position))
        {
            // if a quizz already exists at this position.
            if (isset($this->quizzes[$position])) {
                if ($position === 0) {
                    array_unshift($this->quizzes, $quizz);
                }
                elseif (sizeof($this->quizzes) <= $position) {
                    array_push($this->quizzes, $quizz);
                }
                if (isset($this->quizzes[$position])) {
                    $this->quizzes = array_slice($this->quizzes, 0, $position) + array($position => $quizz) + array_slice($this->quizzes, $position);
                }
            }
            else {
                $this->quizzes[$position] = $quizz;
            }
        }
        else {
            $this->quizzes[] = $quizz;
        }
    }

    /**
     * Set technical ID.
     * @param $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * Get technical ID.
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set author.
     * @param \FOS\UserBundle\Model\UserInterface $user
     * @return $this
     */
    public function setAuthor(UserInterface $user)
    {
        $this->author = $user;
        return $this;
    }

    /**
     * Get current author.
     * @return \FOS\UserBundle\Model\UserInterface
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * Set private status.
     * @param $private
     * @return $this
     */
    public function setPrivate($private)
    {
        $this->private = (bool) $private;
        return $this;
    }

    /**
     * Get private status.
     * @return bool
     */
    public function getPrivate()
    {
        return $this->private;
    }

    /**
     * Is private status.
     * @return bool
     */
    public function isPrivate()
    {
        return (bool) $this->private;
    }

    /**
     * Set private code.
     * @param $code
     * @return $this
     */
    public function setPrivateCode($code)
    {
        $this->privateCode = $code;
        return $this;
    }

    /**
     * Get private code.
     * @return string
     */
    public function getPrivateCode()
    {
        return $this->privateCode;
    }

    /**
     * Set address.
     * @param Address $address
     * @return $this
     */
    public function setAddress(Address $address)
    {
        $this->address = $address;
        return $this;
    }

    /**
     * Get address.
     * @return Address
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set coordinates.
     * @param Coordinates $loc
     * @return $this
     */
    public function setCoordinates(Coordinates $loc)
    {
        $this->coordinates = $loc;
        return $this;
    }

    /**
     * Get coordinates.
     * @return Coordinates
     */
    public function getCoordinates()
    {
        return $this->coordinates;
    }

    /**
     * Set accuracy
     * @param $acc
     * @return $this
     */
    public function setAccuracy($acc)
    {
        $this->accuracy = $acc;
        return $this;
    }

    /**
     * Get accuracy.
     * Return int
     */
    public function getAccuracy()
    {
        return $this->accuracy;
    }

    /**
     * Get image related to contest
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set contest's image
     */
    public function setImage($image) 
    {
        $this->image = $image;
        return $this;
    }

    /**
     * Get image path related to contest
     */
    public function getImagePath()
    {
        return $this->imagePath;
    }

    /**
     * Set contest's image path
     */
    public function setImagePath($image) 
    {
        $this->imagePath = $image;
        return $this;
    }
}
