<?php
namespace Quizz\Bundle\ModelBundle\Model;

/**
 * Interface to be implemented by contest managers.
 */
interface ContestManagerInterface
{
    /**
     * @param  string          $id
     * @return ContestInterface
     */
    public function findContestById($id);

    /**
     * Finds one contest by the given criteria
     *
     * @param  array           $criteria
     * @return ContestInterface
     */
    public function findContestBy(array $criteria);

    /**
     * Finds contests by the given criteria
     *
     * @param array $criteria
     *
     * @return array of ContestInterface
     */
    public function findContestsBy(array $criteria);

    /**
     * Creates an empty contest instance
     *
     * @param  bool   $id
     * @return ContestInterface
     */
    public function createContest($id = null);

    /**
     * Saves a contest
     *
     * @param ContestInterface $contest
     */
    public function saveContest(ContestInterface $contest);

    /**
     * Checks if the contest was already persisted before, or if it's a new one.
     *
     * @param ContestInterface $contest
     *
     * @return boolean True, if it's a new Contest
     */
    public function isNewContest(ContestInterface $contest);

    /**
     * Returns the Contest manager fully qualified class name
     *
     * @return string
     */
    public function getClass();
}
