<?php
namespace Quizz\Bundle\ModelBundle\Model;

abstract class Address {

    /**
     * @var string
     */
    protected $houseNumber;

    /**
     * @var string
     */
    protected $publicBuilding;

    /**
     * @var string
     */
    protected $road;

    /**
     * @var string
     */
    protected $suburb;

    /**
     * @var string
     */
    protected $city;

    /**
     * @var string
     */
    protected $county;

    /**
     * @var string
     */
    protected $stateDistrict;

    /**
     * @var string
     */
    protected $state;

    /**
     * @var string
     */
    protected $postcode;

    /**
     * @var String
     */
    protected $country;

    /**
     * @var string
     */
    protected $countryCode;

    /**
     * @var string
     */
    protected $fullAddress;

    /**
     * @var string
     */
    protected $typeAddress;

    /**
     * @var String
     */
    protected $neighbourhood;

    /**
     * @var $administrative
     */
    protected $administrative;

    /**
     * Set city name
     * @param $cityName
     * @return $this
     */
    public function setCity($cityName)
    {
        $this->city = $cityName;
        return $this;
    }

    /**
     * Return city name.
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }
    /**
     * Set house number
     * @param int $houseNumber
     * @return \Quizz\Bundle\ModelBundle\Model\Address
     */
    public function setHouseNumber($houseNumber) {
        $this->houseNumber = $houseNumber;
        return $this;
    }

    /**
     * Return house number
     * @return string
     */
    public function getHouseNumber() {
        return $this->houseNumber;
    }

    /**
     * Set road
     * @param string $road
     * @return \Quizz\Bundle\ModelBundle\Model\Address
     */
    public function setRoad($road) {
        $this->road = $road;
        return $this;
    }

    /**
     * Return road
     * @return string
     */
    public function getRoad() {
        return $this->road;
    }

    /**
     * Set suburb
     * @param string $suburb
     * @return \Quizz\Bundle\ModelBundle\Model\Address
     */
    public function setSuburb($suburb) {
        $this->suburb = $suburb;
        return $this;
    }

    /**
     * Return suburb
     * @return string
     */
    public function getSuburb() {
        return $this->suburb;
    }

    /**
     * Set county
     * @param string $county
     * @return $this
     */
    public function setCounty($county) {
        $this->county = $county;
        return $this;
    }

    /**
     * Return county
     * @return string
     */
    public function getCounty() {
        return $this->county;
    }

    /**
     * Set stateDistrict
     * @param string $stateDistrict
     * @return $this
     */
    public function setStateDistrict($stateDistrict) {
        $this->stateDistrict = $stateDistrict;
        return $this;
    }

    /**
     * Return stateDistrict
     * @return string
     */
    public function getStateDistrict() {
        return $this->stateDistrict;
    }

    /**
     * Set state
     * @param string $state
     * @return $this
     */
    public function setState($state) {
        $this->state = $state;
        return $this;
    }

    /**
     * Return state
     * @return string
     */
    public function getState() {
        return $this->state;
    }

    /**
     * Set postcode
     * @param string $postcode
     * @return $this
     */
    public function setPostcode($postcode) {
        $this->postcode = $postcode;
        return $this;
    }

    /**
     * Return postcode
     * @return string
     */
    public function getPostcode() {
        return $this->postcode;
    }

    /**
     * Set country
     * @param string $country
     * @return \Quizz\Bundle\ModelBundle\Model\Address
     */
    public function setCountry($country) {
        $this->country = $country;
        return $this;
    }

    /**
     * Return country
     * @return string
     */
    public function getCountry() {
        return $this->country;
    }

    /**
     * Set countryCode
     * @param string $countryCode
     * @return \Quizz\Bundle\ModelBundle\Model\Address
     */
    public function setCountryCode($countryCode) {
        $this->countryCode = $countryCode;
        return $this;
    }

    /**
     * Return country Code
     * @return string
     */
    public function getCountryCode() {
        return $this->countryCode;
    }

    /**
     * Set fullAddress
     * @param string $fullAddress
     * @return \Quizz\Bundle\ModelBundle\Model\Address
     */
    public function setFullAddress($fullAddress) {
        $this->fullAddress = $fullAddress;
        return $this;
    }

    /**
     * Return fullAddress
     * @return string
     */
    public function getFullAddress() {
        return $this->fullAddress;
    }

    /**
     * Set typeAddress
     * @param string $typeAddress
     * @return \Quizz\Bundle\ModelBundle\Model\Address
     */
    public function setTypeAddress($typeAddress) {
        $this->typeAddress = $typeAddress;
        return $this;
    }

    /**
     * Return typeAddress
     * @return string
     */
    public function getTypeAddress() {
        return $this->typeAddress;
    }

    /**
     * Set publicBuilding
     * @param string $publicBuilding
     * @return \Quizz\Bundle\ModelBundle\Model\Address
     */
    public function setPublicBuilding($publicBuilding) {
        $this->publicBuilding = $publicBuilding;
        return $this;
    }

    /**
     * Return publicBuilding
     * @return string
     */
    public function getPublicBuilding() {
        return $this->publicBuilding;
    }

    /**
     * Set neighbourhood
     * @param string $neighbourhood
     * @return \Quizz\Bundle\ModelBundle\Model\Address
     */
    public function setNeighbourhood($neighbourhood) {
        $this->neighbourhood = $neighbourhood;
        return $this;
    }

    /**
     * Return neighbourhood
     * @return string
     */
    public function getNeighbourhood() {
        return $this->neighbourhood;
    }

    /**
     * Set administrative
     * @param string $administrative
     * @return \Quizz\Bundle\ModelBundle\Model\Address
     */
    public function setAdministrative($administrative) {
        $this->administrative = $administrative;
        return $this;
    }

    /**
     * Return administrative
     * @return string
     */
    public function getAdministrative() {
        return $this->administrative;
    }
}
