<?php
namespace Quizz\Bundle\ModelBundle\Model;

use Quizz\Bundle\ModelBundle\Events;
use Quizz\Bundle\ModelBundle\Event\AnswerEvent;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Abstract AnswerManager
 */
abstract class AnswerManager implements AnswerManagerInterface
{
    /**
     * @var
     */
    protected $dispatcher;

    /**
     * Constructor.
     * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $dispatcher
     */
    public function __construct(EventDispatcherInterface $dispatcher)
    {
        $this->dispatcher = $dispatcher;
    }

    /**
     * Finds an answer by id.
     *
     * @param  $id
     * @return AnswerInterface
     */
    public function findAnswerById($id)
    {
        return $this->findAnswerBy(array('id' => $id));
    }

    /**
     * Creates a answer object.
     *
     * @param QuizzInterface $quizz
     * @param ContestInterface $contest
     * @return AnswerInterface
     */
    public function createAnswer(QuizzInterface $quizz, ContestInterface $contest)
    {
        $class = $this->getClass();
        $answer = new $class($quizz, $contest);
        $event = new AnswerEvent($answer);
        $this->dispatcher->dispatch(Events::ANSWER_CREATE, $event);
        return $answer;
    }

    /**
     * Deletes a answer object.
     * @param  AnswerInterface $answer
     * @return mixt
     */
    public function deleteAnswer(AnswerInterface $answer)
    {
        return $this->doDeleteAnswer($answer);

    }

    /**
     * Persists $answer.
     * @param AnswerInterface $answer
     */
    public function saveAnswer(AnswerInterface $answer)
    {
        if (null === $answer->getContest()) {
            throw new \InvalidArgumentException('Answer passed into saveAnswer must have a contest');
        }
        if (null === $answer->getQuizz()) {
            throw new \InvalidArgumentException('Answer passed into saveAnswer must have a quizz');
        }
        $event = new AnswerEvent($answer);
        $isNewAnswer = $this->isNewAnswer($answer);
        if ($isNewAnswer)  {
          $this->dispatcher->dispatch(Events::ANSWER_NEW_PRE_PERSIST, $event);
        }
        else {
          $this->dispatcher->dispatch(Events::ANSWER_PRE_PERSIST, $event);
        }
        $this->doSaveAnswer($answer);
        $event = new AnswerEvent($answer);
        if ($isNewAnswer)  {
          $this->dispatcher->dispatch(Events::ANSWER_NEW_POST_PERSIST, $event);
        }
        else {
          $this->dispatcher->dispatch(Events::ANSWER_POST_PERSIST, $event);
        }
    }

    /**
     * Performs the persistence of the answer.
     *
     * @abstract
     * @param AnswerInterface $answer
     */
    abstract protected function doSaveAnswer(AnswerInterface $answer);

    abstract protected function doDeleteAnswer(AnswerInterface $answer);
}
