<?php
/**
 * User: Gibbs
 * Date: 22/03/2015
 * Time: 18:25
 */

namespace Quizz\Bundle\ModelBundle\Model;

use FOS\UserBundle\Document\User;

abstract class Answer implements AnswerInterface
{
    /**
     * @var mixed
     */
    protected $id;

    /**
     * The answer.
     *
     * @var string
     */
    protected $answer;

    /**
     * @var ContestInterface
     */
    protected $contest;

    /**
     * @var QuizzInterface
     */
    protected $quizz;

    /**
     * @var UserInterface
     */
    protected $voter;

    /**
     * @var DateTime
     */
    protected $created;

    /**
     * @var DateTime
     */
    protected $changed;

    /**
     * @var
     */
    protected $revisions = array();

    public function __construct(QuizzInterface $quizz = null, ContestInterface $contest = null)
    {
        if (isset($quizz)) {
            $this->quizz = $quizz;
        }
        if (isset($contest)) {
            $this->contest = $contest;
        }
        $this->created = new \DateTime();
        $this->changed = new \DateTime();
    }

    /**
     * Save current revision.
     * @return $this
     */
    public function addRevision()
    {
        if ($this->answer)
        {
            $this->revisions[] = array('changed' => $this->changed, 'answer' => $this->answer);
        }
        return $this;
    }

    /**
     * Return the vote unique id
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Gets the quizz this vote belongs to.
     * @return QuizzInterface
     */
    public function getQuizz()
    {
        return $this->quizz;
    }

    /**
     * Sets the quizz this vote belongs to.
     *
     * @param  QuizzInterface $quizz
     * @return $this
     */
    public function setQuizz(QuizzInterface $quizz)
    {
        $this->quizz = $quizz;
        return $this;
    }

    /**
     * Gets the contest this vote belongs to.
     * @return ContestInterface
     */
    public function getContest()
    {
        return $this->contest;
    }

    /**
     * Sets the contest this vote belongs to.
     *
     * @param  ContestInterface $contest
     * @return $this
     */
    public function setContest(ContestInterface $contest)
    {
        $this->contest = $contest;
        return $this;
    }

    /**
     * Set current voter
     * @param User $voter
     * @return $this
     **/
    public function setVoter(User $voter)
    {
        $this->voter = $voter;
        return $this;
    }

    /**
     * @return User $voter
     **/
    public function getVoter()
    {
        return $this->voter;
    }

    /**
     * @return string The votes value.
     */
    public function getAnswer()
    {
        return $this->answer;
    }

    /**
     * Set the user's answer
     * @param string $value
     * @return $this
     */
    public function setAnswer($value)
    {
        $this->answer = trim($value);
        return $this;
    }

    public function prePersist()
    {
        $this->changed = new \DateTime();
    }
}
