<?php
/**
 * Created by PhpStorm.
 * User: Gibbs
 * Date: 21/02/15
 * Time: 14:54
 */
namespace Quizz\Bundle\ModelBundle\Model;

interface QuizzInterface
{
    public function setId($id);
    public function getId();
    public function setQuestion($question);
    public function getQuestion();
    public function setQuestionType($questionType);
    public function getQuestionType();
}
