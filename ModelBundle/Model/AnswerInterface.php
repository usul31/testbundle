<?php
/**
 * User: Gibbs
 * Date: 22/03/2015
 * Time: 18:25
 */

namespace Quizz\Bundle\ModelBundle\Model;

interface AnswerInterface
{
    /**
     * @return mixed unique ID for this answer
     */
    public function getId();

    /**
     * @return ContestInterface
     */
    public function getContest();

    /**
     * @param ContestInterface $contest
     */
    public function setContest(ContestInterface $contest);

    /**
     * @return QuizzInterface
     */
    public function getQuizz();

    /**
     * @param QuizzInterface $quizz
     */
    public function setQuizz(QuizzInterface $quizz);

    /**
     * @return string the selected choice for current quizz
     */
    public function getAnswer();
}
