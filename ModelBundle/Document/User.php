<?php
namespace Quizz\Bundle\ModelBundle\Document;

use FOS\UserBundle\Document\User as BaseUser;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Doctrine\ODM\MongoDB\PersistentCollection;

class User extends BaseUser
{
    const OAUTH_PROVIDER_FACEBOOK = 'facebook';

    protected $firstname;
    protected $lastname;
    protected $gender;
    protected $pseudo;
    protected $private;
    protected $biography;
    protected $currentCity;
    protected $currentCountry;
    protected $favoriteLanguage;
    protected $dob;
    protected $groups = array();
    protected $accessToken = '';
    protected $refreshToken = '';
    protected $picture = NULL;
    protected $picturePath = '';
    protected $pictureUrl = '';
    protected $confirmation = false;
    protected $nbFriends = 0;
    protected $nbFollowers = 0;
    protected $facebookId;
    protected $facebookAccessToken;
    protected $reports;
    protected $nbReports;

    protected $favoriteContests = array();

    /**
     * Nb answers.
     * @var int
     */
    protected $nbAnswers = 0;

    /**
     * Nb created contests.
     * @var int
     */
    protected $nbContests = 0;


    /**
     * This variable is dynamically set.
     * @var boolean
     */
    private $isFriendFriendWithCurrentLoggedUser;


    /**
     * @var DateTime
     */
    protected $created;

    /**
     * @var DateTime
     */
    protected $changed;

    protected $nbVotes = 0;
    protected $nbComments = 0;

    protected $iosDevices = array();
    protected $androidDevices = array();
    protected $facebookPicture;
    /**
     * @var $id
     */
    protected $id;

    public function __construct()
    {
        parent::__construct();
        $this->created = new \DateTime();
        $this->changed = new \DateTime();
        $this->groups = new \Doctrine\Common\Collections\ArrayCollection();
        $this->private = false;
    }

    /**
     * Add favorite contest.
     * @param $favoriteContest
     * @return $this
     */
    public function addFavoriteContest($favoriteContest)
    {
        if ($this->favoriteContests instanceof PersistentCollection) {
            foreach($this->favoriteContests as $key => $localFavorite) {
                // Contest is already in favorite.
                if($localFavorite->getContest() === $favoriteContest->getContest()) {
                    return $this;
                }
            }
        }
        $this->favoriteContests[] = $favoriteContest;
        return $this;
    }

    /**
     * Check if contest is favorite.
     * @param $contest
     * @return bool
     */
    public function isFavoriteContest($contest)
    {
        if ($this->favoriteContests instanceof PersistentCollection) {
            foreach($this->favoriteContests as $key => $localFavorite) {
                // Contest is already in favorite.
                if($localFavorite->getContest() === $contest) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Remove favorite contest.
     * @param $favoriteContest
     * @return $this
     */
    public function removeFavoriteContest($favoriteContest)
    {
        if ($this->favoriteContests instanceof PersistentCollection) {
            foreach($this->favoriteContests as $key => $localFavorite) {
                // Contest is already in favorite.
                if($localFavorite->getContest() === $favoriteContest->getContest()) {
                    $this->favoriteContests->remove($key);
                    return $this;
                }
            }
        }
        return $this;
    }

    public function getFavoriteContests()
    {
        return $this->favoriteContests;
    }
    /**
     * Increments nb contests.
     * @return $this
     */
    public function incrementNbContests()
    {
        $this->nbContests++;
        return $this;
    }

    /**
     * Get nb contests.
     * @return int
     */
    public function getNbContests()
    {
        return $this->nbContests;
    }

    /**
     * Increments nb answers.
     * @return $this
     */
    public function incrementNbAnswers()
    {
        $this->nbAnswers++;
        return $this;
    }

    /**
     * Get nb answers.
     * @return int
     */
    public function getNbAnswers()
    {
        return $this->nbAnswers;
    }

    public function setFacebookPicture($facebookPicture) {
        $this->facebookPicture = $facebookPicture;
        return $this;
    }

    public function getFacebookPicture() {
        return $this->facebookPicture;
    }

    /**
     * Serializes the user.
     *
     * The serialized data have to contain the fields used by the equals method and the username.
     *
     * @return string
     */
    public function toArray()
    {
        return array(
            'id' => $this->getId(),
            'username' => $this->username,
            'email' => $this->email,
            'nb_votes' => $this->getNbVotes(),
            'nb_comments' => $this->getNbComments(),
            'current_country' => $this->getCurrentCountry(),
            'nb_friends' => $this->getNbFriends(),
            'lastname' => $this->getLastname(),
            'nb_followers' => $this->getNbFollowers(),
            'dob' => $this->getDob(),
            'private' => $this->getPrivate(),
            'picture_url' => $this->getPictureUrl(),
            'facebook_picture' => $this->getFacebookPicture(),
            'current_city' => $this->getCurrentCity(),
            'pseudo' => $this->getPseudo(),
            'username_canonical' => $this->getUsernameCanonical(),
            'gender' => $this->getGender(),
            'firstname' => $this->getFirstname(),
            'favorite_language' => $this->getFavoriteLanguage()
        );
    }

    /**
     * @param $isFriend
     */
    public function setFriendFriendWithCurrentLoggedUser($isFriend)
    {
        $this->isFriendFriendWithCurrentLoggedUser = (bool) $isFriend;
        return $this;
    }

    /**
     * @return bool
     */
    public function isFriendFriendWithCurrentLoggedUser()
    {
        return $this->isFriendFriendWithCurrentLoggedUser;
    }

    /**
     * Return AndroidDevices array.
     * @return mixed
     */
    public function getAndroidDevices()
    {
        return $this->androidDevices;
    }

    /**
     * Set Android devices array.
     * @param array $devices
     * @return $this
     */
    public function setAndroidDevices(array $devices = array()) {
        $this->androidDevices = $devices;
        return $this;
    }

    /**
     * Add new Android devices.
     * @param $device
     * @return User
     */
    public function addAndroidDevice($device) {
        if (!in_array($device, $this->androidDevices)) {
            $this->androidDevices[] = $device;
        }
        return $this;
    }

    /**
     * @param array $devices
     * @return User
     */
    public function addAndroidDevices(array $devices = array()) {
        foreach ($devices as $device) {
            $this->addAndroidDevice($device);
        }
        return $this;
    }

    /**
     * Return iosDevices array.
     * @return mixed
     */
    public function getIosDevices()
    {
        return $this->iosDevices;
    }

    /**
     * Set ios devices array.
     * @param array $devices
     * @return $this
     */
    public function setIosDevices(array $devices = array()) {
        $this->iosDevices = $devices;
        return $this;
    }

    /**
     * Add new ios devices.
     * @param $device
     * @return User
     */
    public function addIosDevice($device) {
        if (!in_array($device, $this->iosDevices)) {
            $this->iosDevices[] = $device;
        }
        return $this;
    }

    /**
     * @param array $devices
     * @return User
     */
    public function addIosDevices(array $devices = array()) {
        foreach ($devices as $device) {
            $this->addIosDevice($device);
        }
        return $this;
    }

    /**
     * update changed date.
     * @return User
     */
    public function updateChanged(){
        $this->changed = new \DateTime();
        return $this;
    }

    /**
     * set created date
     * @param \DateTime $created
     * @return User
     **/
    public function setCreated(\DateTime $created)
    {
        $this->created = $created;
        return $this;
    }

    /**
     * Get created date
     * @return DateTime
     **/
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set changed date
     * @param DateTime $changed
     * @return User
     **/
    public function setChanged(\DateTime $changed)
    {
        $this->changed = $changed;
        return $this;
    }

    /**
     * Get changed date
     * @return date
     **/
    public function getChanged()
    {
        return $this->changed;
    }

    public function getNbFriends()
    {
        return $this->nbFriends;
    }

    public function setNbFriends($nb)
    {
        $this->nbFriends = $nb;
        return $this;
    }

    public function incNbFriends()
    {
        $this->nbFriends++;
        return $this;
    }

    public function decNbFriends()
    {
        $this->nbFriends--;
        return $this;
    }

    public function getNbFollowers()
    {
        return $this->nbFollowers;
    }

    public function setNbFollowers($nb)
    {
        $this->nbFollowers = $nb;
        return $this;
    }

    public function incNbFollowers()
    {
        $this->nbFollowers++;
        return $this;
    }

    public function decNbFollowers()
    {
        $this->nbFollowers--;
        return $this;
    }


    public function getConfirmation()
    {
        return $this->confirmation;
    }

    public function setConfirmation($confirmation)
    {
        $this->confirmation = (bool)$confirmation;
        return $this;
    }

    /**
     * Increments number of votes.
     * @return $this
     */
    public function incrementNbVotes()
    {
        $this->nbVotes++;
        return $this;
    }

    /**
     * Decrease number of votes.
     * @return $this
     */
    public function decNbVotes()
    {
        $this->nbVotes--;
        if ($this->nbVotes < 0) $this->nbVotes = 0;
        return $this;
    }

    /**
     * Increments number of comments.
     * @return $this
     */
    public function incrementNbComments()
    {
        $this->nbComments++;
        return $this;
    }

    /**
     * Decrease number of comments.
     * @return $this
     */
    public function decNbComments()
    {
        $this->nbComments--;
        if ($this->nbComments < 0) $this->nbComments = 0;
        return $this;
    }

    /**
     * Return number of votes.
     * @return int
     */
    public function getNbVotes()
    {
        return $this->nbVotes;
    }

    /**
     * Return number of comments.
     * @return int
     */
    public function getNbComments()
    {
        return $this->nbComments;
    }

    /**
     * Set dob
     *
     * @param date $dob
     */
    public function setDob($dob)
    {
        $this->dob = $dob;
    }

    /**
     * Get dob
     *
     * @return date $dob
     */
    public function getDob()
    {
        return $this->dob;
    }

    /**
     * Set gender
     *
     * @param string $gender
     */
    public function setGender($gender)
    {
        $this->gender = $gender;
    }

    /**
     * Get gender
     *
     * @return string $gender
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Add groups
     *
     * @param \Quizz\Bundle\ModelBundle\Document\Group $groups
     */
    public function addGroups(\Quizz\Bundle\ModelBundle\Document\Group $groups)
    {
        $this->groups->add($groups);
    }

    /**
     * Get groups
     *
     * @return \Doctrine\Common\Collections\Collection $groups
     */
    public function getGroups()
    {
        return $this->groups;
    }

    /**
     * Get id
     *
     * @return custom_id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set firstname
     *
     * @param string $firstname
     * @return User
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;
        return $this;
    }

    /**
     * Get firstname
     *
     * @return string $firstname
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set lastname
     *
     * @param string $lastname
     * @return User
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;
        return $this;
    }

    /**
     * Get lastname
     *
     * @return string $lastname
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Get Access Token
     *
     * @return string $accessToken
     */
    public function getAccessToken()
    {
        return $this->accessToken;
    }

    /**
     * Set Access Token
     *
     * @param string $accessToken
     * @return User
     */
    public function setAccessToken($accessToken)
    {
        $this->accessToken = $accessToken;
        // Update last login
        $this->setLastLogin(new \DateTime());
        return $this;
    }

    /**
     * Get Refresh Token
     *
     * @return string $refreshToken
     */
    public function getRefreshToken()
    {
        return $this->refreshToken;
    }

    /**
     * Set Refresh Token
     *
     * @param string $refreshToken
     * @return User
     */
    public function setRefreshToken($refreshToken)
    {
        $this->refreshToken = $refreshToken;
        return $this;
    }

    /**
     * Sets profile image.
     *
     * @param UploadedFile $file
     */
    public function setPicture(UploadedFile $file = NULL)
    {
        $this->picture = $file;
        return $this;
    }

    /**
     * Get profile image.
     *
     * @return UploadedFile
     */
    public function getPicture()
    {
        return $this->picture;
    }

    /**
     * Sets profile image path.
     *
     * @param string $file
     */
    public function setPictureUrl($path = NULL)
    {
        $this->pictureUrl = $path;
        return $this;
    }

    /**
     * Get profile image path.
     *
     * @return string
     */
    public function getPictureUrl()
    {
        return $this->pictureUrl;
    }


    /**
     * Sets profile image path.
     *
     * @param string $file
     */
    public function setPicturePath($path = NULL)
    {
        $this->picturePath = $path;
        return $this;
    }

    /**
     * Get profile image path.
     *
     * @return string
     */
    public function getPicturePath()
    {
        return $this->picturePath;
    }

    public function upload()
    {
        // no new image uploaded.
        if (NULL === $this->getPicture()) {
            return FALSE;
        }
        $filename = sha1(uniqid(mt_rand(), TRUE)) . '.' . $this->getPicture()->guessExtension();
        $uploadDir = DIRECTORY_SEPARATOR . implode(DIRECTORY_SEPARATOR, array_slice(str_split($filename, 1), 0, 10));
        // move takes the target directory and then the
        // target filename to move to
        $this->getPicture()->move($this->getUploadRootDir() . DIRECTORY_SEPARATOR . $this->getUploadDir() . $uploadDir, $filename);
        $this->picturePath = $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $this->getUploadDir() . $uploadDir . DIRECTORY_SEPARATOR . $filename;
        $this->pictureUrl = $this->getUploadDir() . $uploadDir . DIRECTORY_SEPARATOR . $filename;
        $this->picture = NULL;
        return TRUE;
    }

    /**
     * Set upload dir
     * @param null $path
     * @return $this
     */
    public function setUploadDir($path = NULL)
    {
        $this->uploadDir = $path;
        return $this;
    }

    /**
     * Get upload dir
     */
    public function getUploadDir()
    {
        return $this->uploadDir;
    }

    /**
     * Set upload root dir
     * @param null $path
     * @return $this
     */
    public function setUploadRootDir($path = NULL)
    {
        $this->uploadRootDir = $path;
        return $this;
    }

    /**
     * Get upload root dir
     */
    public function getUploadRootDir()
    {
        return $this->uploadRootDir;
    }

    /**
     * Set pseudo
     * @param $pseudo
     * @return $this
     */
    public function setPseudo($pseudo)
    {
        $this->pseudo = $pseudo;
        return $this;
    }

    /**
     * Get pseudo
     * @return mixed
     */
    public function getPseudo()
    {
        return $this->pseudo;
    }

    /**
     * Set private or not profile.
     * @param bool $private
     * @return $this
     */
    public function setPrivate($private = TRUE)
    {
        $this->private = (boolean)$private;
        return $this;
    }

    /**
     * Get private parameters.
     * @return mixed
     */
    public function getPrivate()
    {
        return $this->private;
    }

    /**
     * Return if profile is private.
     * @return bool
     */
    public function isPrivate()
    {
        return (boolean)$this->private;
    }

    /**
     * Set current city.
     * @param $currentCity
     * @return $this
     */
    public function setCurrentCity($currentCity)
    {
        $this->currentCity = $currentCity;
        return $this;
    }

    /**
     * Get current city
     * @return mixed
     */
    public function getCurrentCity()
    {
        return $this->currentCity;
    }

    /**
     * Set current country.
     * @param $currentCountry
     * @return $this
     */
    public function setCurrentCountry($currentCountry)
    {
        $this->currentCountry = $currentCountry;
        return $this;
    }

    /**
     * Get current country
     * @return mixed
     */
    public function getCurrentCountry()
    {
        return $this->currentCountry;
    }

    /**
     * Set favorite language.
     * @param $favoriteLanguage
     * @return $this
     */
    public function setFavoriteLanguage($favoriteLanguage)
    {
        $this->favoriteLanguage = $favoriteLanguage;
        return $this;
    }

    /**
     * Get favorite language
     * @return mixed
     */
    public function getFavoriteLanguage()
    {
        return $this->favoriteLanguage;
    }



    /**
     * Set biography.
     * @param $biography
     * @return $this
     */
    public function setBiography($biography)
    {
        $this->biography = $biography;
        return $this;
    }

    /**
     * Get biography.
     * @return mixed
     */
    public function getBiography()
    {
        return $this->biography;
    }

    /**
     * Return if user should be index or not.
     */
    public function isIndexable()
    {
        return $this->enabled;
    }

    /**
     * @param string $facebookId
     * @return void
     */
    public function setFacebookId($facebookId)
    {
        $this->facebookId = $facebookId;
        return $this;
    }

    /**
     * @return string
     */
    public function getFacebookId()
    {
        return $this->facebookId;
    }

    /**
     * @param string $facebookAccessToken
     * @return void
     */
    public function setFacebookAccessToken($accessToken)
    {
        $this->facebookAccessToken = $accessToken;
        return $this;
    }

    /**
     * @return string
     */
    public function getFacebookAccessToken()
    {
        return $this->facebookAccessToken;
    }

    /**
     * @param Array
     */
    public function setFBData($fbdata)
    {
        if (isset($fbdata['id'])) {
            $this->setFacebookId($fbdata['id']);
            $this->addRole('ROLE_FACEBOOK');
            $this->addRole('ROLE_USER');
        }
        if (isset($fbdata['first_name'])) {
            $this->setFirstname($fbdata['first_name']);
        }
        if (isset($fbdata['last_name'])) {
            $this->setLastname($fbdata['last_name']);
        }
        if (isset($fbdata['name'])) {
            $this->setPseudo($fbdata['name']);
        }
        if (isset($fbdata['picture']) && is_object($fbdata['picture']) && isset($fbdata['picture']->data) && is_object($fbdata['picture']->data) && isset($fbdata['picture']->data->url)) {
            $this->setFacebookPicture($fbdata['picture']->data->url);
        }
        if (isset($fbdata['email'])) {
            $this->setEmail($fbdata['email']);
        }
        else {
            $this->setEmail($fbdata['id'] . '@facebook.com');
        }
    }
}
