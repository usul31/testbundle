<?php
namespace Quizz\Bundle\ModelBundle\Document;

use Doctrine\ODM\MongoDB\DocumentManager;
use Quizz\Bundle\ModelBundle\Model\AnswerManager as BaseAnswerManager;
use Quizz\Bundle\ModelBundle\Model\QuizzInterface;
use Quizz\Bundle\ModelBundle\Model\ContestInterface;
use Quizz\Bundle\ModelBundle\Model\AnswerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use FOS\UserBundle\Model\UserInterface;

/**
 * Default ODM AnswerManager.
 */
class AnswerManager extends BaseAnswerManager
{
    /**
     * @var DocumentManager
     */
    protected $dm;

    /**
     * @var DocumentRepository
     */
    protected $repository;

    /**
     * @var string
     */
    protected $class;

    /**
     * Constructor.
     *
     * @param DocumentManager $dm
     * @param string          $class
     */
    public function __construct(EventDispatcherInterface $dispatcher, DocumentManager $dm, $class)
    {
        parent::__construct($dispatcher);

        $this->dm = $dm;
        $this->repository = $dm->getRepository($class);

        $metadata = $dm->getClassMetadata($class);
        $this->class = $metadata->name;
    }

    /**
     * Retrieves latest answer for quizz $quizz
     * @param QuizzInterface $quizz
     * @param $nbResults
     * @param $page
     * @return mixt
     */
    public function findLatestAnswersForQuizz(QuizzInterface $quizz, $nbResults, $page)
    {
        $query = $this->repository->createQueryBuilder()->find();
        $query->field('quizz')->equals(($quizz->getId()));
        $query->sort('changed', 'DESC');
        return $query->skip($nbResults * $page + min(1, $page))->limit($nbResults)->getQuery()->execute();
    }

    /**
     * Get user $user's answer for contest $contest.
     * @param UserInterface $user
     * @param ContestInterface $contest
     * @return mixt
     */
    public function getAnswerForUserAndContest(UserInterface $user, ContestInterface $contest)
    {
        return $this->findAnswersBy(array(
            'contest' => $contest->getId(),
            'voter' => $user->getId()
        ));
    }

    /**
     * Get user $user's answer for quizz $quizz.
     * @param UserInterface $user
     * @param QuizzInterface $quizz
     * @return mixt
     */
    public function getAnswerForUserAndQuizz(UserInterface $user, QuizzInterface $quizz)
    {
        return $this->findAnswerBy(array(
            'quizz' => $quizz->getId(),
            'voter' => $user->getId()
        ));
    }

    /**
     * Persists an answer.
     * @param AnswerInterface $answer
     */
    protected function doSaveAnswer(AnswerInterface $answer)
    {
        $this->dm->persist($answer);
        $this->dm->flush();
    }

    /**
     * Finds an answer by specified criteria.
     * @param  array         $criteria
     * @return AnswerInterface
     */
    public function findAnswerBy(array $criteria)
    {
        return $this->repository->findOneBy($criteria);
    }

    /**
     * {@inheritDoc}
     */
    public function findAnswersBy(array $criteria)
    {
        return $this->repository->findBy($criteria);
    }

    /**
     * Deletes an answer
     * @param AnswerInterface $answer
     */
    protected function doDeleteAnswer(AnswerInterface $answer)
    {
        $this->dm->remove($answer);
        $this->dm->flush();
    }

    /**
     * Returns the fully qualified spot vote class name
     *
     * @return string
     */
    public function getClass()
    {
        return $this->class;
    }

    /**
     * {@inheritDoc}
     */
    public function isNewAnswer(AnswerInterface $answer)
    {
        return !$this->dm->getUnitOfWork()->isInIdentityMap($answer);
    }
}
