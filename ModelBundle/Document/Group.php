<?php
namespace Quizz\Bundle\ModelBundle\Document;

use FOS\UserBundle\Document\Group as BaseGroup;

class Group extends BaseGroup
{

    /**
     * @var $id
     */
    protected $id;


    /**
     * Get id
     *
     * @return custom_id $id
     */
    public function getId()
    {
        return $this->id;
    }
}
