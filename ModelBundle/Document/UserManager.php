<?php
namespace Quizz\Bundle\ModelBundle\Document;

use Doctrine\ODM\MongoDB\DocumentManager;
use FOS\UserBundle\Doctrine\UserManager as BaseUserManager;
use FOS\UserBundle\Util\CanonicalizerInterface;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Quizz\Bundle\ModelBundle\Events as Events;
use Quizz\Bundle\ModelBundle\Event\UserEvent;

/**
 *
 */
class UserManager extends BaseUserManager
{
    protected $dm;
    protected $dispatcher;

    public function __construct(EventDispatcherInterface $dispatcher, EncoderFactoryInterface $encoderFactory, CanonicalizerInterface $usernameCanonicalizer, CanonicalizerInterface $emailCanonicalizer, DocumentManager $dm, $class)
    {
        $this->dispatcher = $dispatcher;
        parent::__construct($encoderFactory, $usernameCanonicalizer, $emailCanonicalizer, $dm, $class);

        $this->dm = $dm;
    }

    /**
     * Find users by a list of ids
     * @param array $userIds array of user ids.
     */
    public function findUsersByIds(array $userIds = array())
    {
        return $this->repository->createQueryBuilder()
            ->find()
            ->field('_id')->in($userIds)
            ->getQuery()
            ->execute();
    }

    /**
     * Retrieve a user and dispatch a event.
     * @param string $usernameOrEmail
     * @return mixte
     */
    public function findUserByUsernameOrEmailSpecial($usernameOrEmail)
    {
        $user = $this->findUserByUsernameOrEmail($usernameOrEmail);

        $event = new UserEvent($user);
        $this->dispatcher->dispatch(Events::USER_POST_LOAD, $event);
        return $user;
    }

    /**
     * Return the list of sposts with reports
     * @param int $page
     * @param int $limit
     */
    public function getUsersWithReports($page = 0, $limit = 10) {
        return $this->repository->createQueryBuilder()
            ->find()
            ->field('nbReports')->notEqual(0)
            ->field('nbReports')->exists(true)
            ->field('enabled')->equals(true)
            ->sort('nbReports', 'DESC')
            ->skip($page * $limit + min(1, $page))->limit($limit)
            ->getQuery()->execute();
    }

    /**
     * Retrieve all tokens from users who have activites on the current users.
     * @param string $userIds
     * @return array
     */
    public function getUsersTokens($userIds)
    {
        $iosTokens = array();
        $androidTokens = array();

        $activityUsers = $this->findUsersByIds($userIds);
        foreach($activityUsers as $activityUser) {
            $iosTokens += $activityUser->getUser()->getIosDevices();
            $androidTokens += $activityUser->getUser()->getAndroidDevices();
        }
        return array(
            'ios' => array_unique($iosTokens),
            'android' => array_unique($androidTokens)
        );
    }
}
