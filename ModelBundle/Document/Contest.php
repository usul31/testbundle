<?php
namespace Quizz\Bundle\ModelBundle\Document;

use Quizz\Bundle\ModelBundle\Model\Contest as BaseContest;

/**
 * Contest document.
 */
class Contest extends BaseContest
{
    /**
     * Is in favorite list for current user ?
     * @var boolean
     */
    private $isFavorite = false;

    /**
     * has current user started contest.
     * @var boolean
     */
    private $userStatus = 'empty';

    /**
     * Set user status
     * @param $status
     * @return $this
     */
    public function setUserStatus($status) {
        $this->userStatus = $status;
        return $this;
    }

    /**
     * Get current logged user status.
     * @return bool
     */
    public function getUserStatus() {
        return $this->userStatus;
    }

    /**
     * Set favorite
     * @param $favorite
     * @return $this
     */
    public function setFavorite($favorite)
    {
        $this->isFavorite = (bool) $favorite;
        return $this;
    }

    public function isFavorite()
    {
        return $this->isFavorite;
    }

    /**
     * Upload picture
     * @return bool
     */
    public function upload()
    {
        if ($info = parent::upload()) {
            $this->image = $info['image'];
            $this->imagePath = $info['path'];

            return true;
        }

        return false;
    }
}
