<?php
namespace Quizz\Bundle\ModelBundle\Document;

use Quizz\Bundle\ModelBundle\Model\Quizz as BaseQuizz;

/**
 * Quizz document.
 */
class Quizz extends BaseQuizz
{
    /**
     * @param int $response
     * @param bool $question
     * @return bool
     */
    public function upload($response = 1, $question = false)
    {
        if ($info = parent::upload()) {
            if ($question) {
                $imageName = 'questionImage';
                $imagePath = 'questionImagePath';
            }
            else {
                $imageName = 'responseChoice' . $response;
                $imagePath = 'responseChoice' . $response . 'Path';
            }
            $this->{$imageName} = $info['image'];
            $this->{$imagePath} = $info['path'];

            return true;
        }

        return false;
    }
}
