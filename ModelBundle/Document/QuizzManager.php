<?php
namespace Quizz\Bundle\ModelBundle\Document;

use Doctrine\ODM\MongoDB\DocumentManager;
use Quizz\Bundle\ModelBundle\Model\QuizzInterface;
use Quizz\Bundle\ModelBundle\Model\QuizzManager as BaseQuizzManager;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;


/**
 * ODM QuizzManager.
 */
class QuizzManager extends BaseQuizzManager
{
    /**
     * @var DocumentManager
     */
    protected $dm;

    /**
     * @var DocumentRepository
     */
    protected $repository;

    /**
     * @var string
     */
    protected $class;

    /**
     * Constructor.
     * @param EventDispatcherInterface $dispatcher
     * @param DocumentManager $dm
     * @param string $class
     */
    public function __construct(EventDispatcherInterface $dispatcher, DocumentManager $dm, $class)
    {
        parent::__construct($dispatcher);
        $this->dm = $dm;
        $this->repository = $dm->getRepository($class);

        $metadata = $dm->getClassMetadata($class);
        $this->class = $metadata->name;
    }

    /**
     * {@inheritDoc}
     */
    public function findQuizzBy(array $criteria)
    {
        return $this->repository->findOneBy($criteria);
    }

    /**
     * {@inheritDoc}
     */
    public function findQuizzesBy(array $criteria)
    {
        return $this->repository->findBy($criteria);
    }

    /**
     * {@inheritDoc}
     */
    public function isNewQuizz(QuizzInterface $quizz)
    {
        return !$this->dm->getUnitOfWork()->isInIdentityMap($quizz);
    }

    /**
     * {@inheritDoc}
     */
    protected function doSaveQuizz(QuizzInterface $quizz)
    {
        $this->dm->persist($quizz);
        $this->dm->flush();
    }

    /**
     * Returns the fully qualified quizz class name
     *
     * @return string
     **/
    public function getClass()
    {
        return $this->class;
    }

    /**
     * Return list of quizz near to the location.
     * @param float $longitude
     * @param float $latitude
     * @param int $limit
     * @param int $page
     *
     * @return Object
     */
    public function findByLocalisation($longitude, $latitude, $limit = 10, $page = 0, $maxDistance = null)
    {
        $query = $this->repository->createQueryBuilder()->find();
        $query->field('coordinates')->geoNear((float)$latitude, (float)$longitude)->spherical(true)->distanceMultiplier(6371000);
        $query->sort('distance', 'ASC');
        if ($maxDistance) {
            $query->maxDistance((int)$maxDistance);
        }
        return $query->skip($page * $limit + min(1, $page))->limit($limit)->getQuery()->execute();
    }


    /**
     * {@inheritDoc}
     */
    protected function doDeleteQuizz(QuizzInterface $quizz)
    {
        $this->dm->remove($quizz);
        $this->dm->flush();
    }
}
