<?php
namespace Quizz\Bundle\ModelBundle\Document;

use Doctrine\ODM\MongoDB\DocumentManager;
use Quizz\Bundle\ModelBundle\Model\ContestInterface;
use Quizz\Bundle\ModelBundle\Model\ContestManager as BaseContestManager;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;


/**
 * ODM QuizzManager.
 */
class ContestManager extends BaseContestManager
{
    /**
     * @var DocumentManager
     */
    protected $dm;

    /**
     * @var DocumentRepository
     */
    protected $repository;

    /**
     * @var string
     */
    protected $class;

    /**
     * Constructor.
     * @param EventDispatcherInterface $dispatcher
     * @param DocumentManager $dm
     * @param string $class
     */
    public function __construct(EventDispatcherInterface $dispatcher, DocumentManager $dm, $class)
    {
        parent::__construct($dispatcher);
        $this->dm = $dm;
        $this->repository = $dm->getRepository($class);

        $metadata = $dm->getClassMetadata($class);
        $this->class = $metadata->name;
    }

    /**
     * {@inheritDoc}
     */
    public function findContestBy(array $criteria)
    {
        return $this->repository->findOneBy($criteria);
    }

    /**
     * {@inheritDoc}
     */
    public function findContestsBy(array $criteria)
    {
        return $this->repository->findBy($criteria);
    }

    /**
     * {@inheritDoc}
     */
    public function isNewContest(ContestInterface $contest)
    {
        return !$this->dm->getUnitOfWork()->isInIdentityMap($contest);
    }

    /**
     * {@inheritDoc}
     */
    protected function doSaveContest(ContestInterface $contest)
    {
        $this->dm->persist($contest);
        $this->dm->flush();
    }

    /**
     * Returns the fully qualified contest class name
     *
     * @return string
     **/
    public function getClass()
    {
        return $this->class;
    }

    /**
     * Get latest published contests.
     * @param int $nbResults
     * @param int $page
     * @return mixed
     */
    public function findLatestQuizzes($nbResults = 10, $page = 0)
    {
        $query = $this->repository->createQueryBuilder()->find();
        $query->field('published')->equals(true);
        $query->sort('created', 'DESC');
        return $query->skip($nbResults * $page + min(1, $page))->limit($nbResults)->getQuery()->execute();
    }

    /**
     * Get latest published contests.
     * @param int $nbResults
     * @param int $page
     * @return mixed
     */
    public function findLatestQuizzesByUser($userId, $nbResults = 10, $page = 0)
    {
        $query = $this->repository->createQueryBuilder()->find();
        $query->field('author')->equals($userId);
        $query->sort('created', 'DESC');
        return $query->skip($nbResults * $page + min(1, $page))->limit($nbResults)->getQuery()->execute();
    }

    /**
     * Return list of contest near to the location.
     * @param float $longitude
     * @param float $latitude
     * @param int $limit
     * @param int $page
     * @param int maxDistance
     *
     * @return Object
     */
    public function findByLocalisation($longitude, $latitude, $limit = 10, $page = 0, $maxDistance = null)
    {
        $query = $this->repository->createQueryBuilder()->find();
        $query->field('coordinates')->geoNear((float)$latitude, (float)$longitude)->spherical(true)->distanceMultiplier(6371000);
        $query->sort('distance', 'ASC');
        $query->field('published')->equals(true);
        if ($maxDistance) {
            $query->maxDistance((int)$maxDistance);
        }
        return $query->skip($page * $limit + min(1, $page))->limit($limit)->getQuery()->execute();
    }


    /**
     * {@inheritDoc}
     */
    protected function doDeleteContest(ContestInterface $contest)
    {
        $this->dm->remove($contest);
        $this->dm->flush();
    }
}
