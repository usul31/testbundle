<?php
namespace Quizz\Bundle\ModelBundle\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Log\LoggerInterface;

/**
 * Assigns a FOS\UserBundle user from the logged in user to a vote.
 *
 */
class QuizzListener implements EventSubscriberInterface
{
    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * Constructor.
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger = null)
    {
        $this->logger = $logger;
    }



    public static function getSubscribedEvents()
    {
        return array(
        );
    }
}
