<?php
namespace Quizz\Bundle\ModelBundle\EventListener;

use Quizz\Bundle\ModelBundle\Events;
use Quizz\Bundle\ModelBundle\Event\AnswerEvent;
use Quizz\Bundle\ModelBundle\Doctrine\UserManager;
use Quizz\Bundle\ModelBundle\Model\QuizzManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Log\LoggerInterface;


/**
 * Assigns a FOS\UserBundle user from the logged in user to a vote.
 *
 */
class AnswerListener implements EventSubscriberInterface
{
    /**
     * @var LoggerInterface
     */
    protected $logger;
    protected $userManager;
    protected $quizzManager;

    /**
     * Constructor.
     * @param LoggerInterface $logger
     */
    public function __construct(UserManager $userManager, QuizzManagerInterface $quizzManager, LoggerInterface $logger = null)
    {
        $this->userManager = $userManager;
        $this->quizzManager = $quizzManager;
        $this->logger = $logger;
    }

    /**
     * @param AnswerEvent $event
     */
    function postAnswerCreate(AnswerEvent $event)
    {
        try
        {
            $author = $event->getAnswer()->getVoter();
            // Increment author answer counter.
            $author->incrementNbAnswers();
            $this->userManager->updateUser($author);
            // Increment quizz answers
            $quizz = $event->getAnswer()->getQuizz();
            $incMethods = 'incNb' . ucfirst($event->getAnswer()->getAnswer());
            $this->logger->critical($incMethods);
            if (method_exists($quizz, $incMethods)) {
                $this->logger->critical('exists !!');
                $quizz->{$incMethods}();
                $this->quizzManager->saveQuizz($quizz);
            }
        }
        catch (\Exception $e)
        {
            $this->logger->critical('[Exception] Failed to increment nb answer with exception :' . $e->getMessage());
        }
    }



    public static function getSubscribedEvents()
    {
        return array(
            Events::ANSWER_NEW_POST_PERSIST => 'postAnswerCreate'
        );
    }
}
