<?php
namespace Quizz\Bundle\ModelBundle\EventListener;

use Quizz\Bundle\ModelBundle\Events;
use Quizz\Bundle\ModelBundle\Event\ContestEvent;
use Quizz\Bundle\ModelBundle\Doctrine\UserManager;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Log\LoggerInterface;

/**
 * Assigns a FOS\UserBundle user from the logged in user to a vote.
 *
 */
class ContestListener implements EventSubscriberInterface
{
    /**
     * @var LoggerInterface
     */
    protected $logger;
    protected $userManager;

    /**
     * Constructor.
     * @param LoggerInterface $logger
     */
    public function __construct(UserManager $userManager, LoggerInterface $logger = null)
    {
        $this->userManager = $userManager;
        $this->logger = $logger;
    }

    /**
     * @param ContestEvent $event
     */
    function postContestCreate(ContestEvent $event)
    {
        try
        {
            $author = $event->getContest()->getAuthor();
            $author->incrementNbContests();
            $this->userManager->updateUser($author);
        }
        catch (\Exception $e)
        {
            $this->logger->critical('[Exception] Failed to increment nb contests with exception :' . $e->getMessage());
        }
    }



    public static function getSubscribedEvents()
    {
        return array(
            Events::CONTEST_NEW_POST_PERSIST => 'postContestCreate'
        );
    }
}
